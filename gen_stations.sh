#!/bin/bash
#KR84, 08.09.2024

echo "using src/A1.html and generate A2.html"
sed 's/A1/A2/g' ./src/A1.html | sed -e 's/<a class="nav" href="A2.html">A2<\/a>|<a class="nav" href="A2.html">A2<\/a>/<a class="nav" href="A1.html">A1<\/a>|<a class="nav" href="A2.html">A2<\/a>/g' > ./src/A2.html


echo "using src/SR1.html and generate SR2,3,4.html's"
sed 's/SR1/SR2/g' ./src/SR1.html | sed -e 's/0060/0140/g' | sed -e 's/Station 1/Station 2/g' | sed -e 's/<a class="nav" href="SR2.html">SR2<\/a>|<a class="nav" href="SR2.html">SR2<\/a>/<a class="nav" href="SR1.html">SR1<\/a>|<a class="nav" href="SR2.html">SR2<\/a>/g' > ./src/SR2.html
sed 's/SR1/SR3/g' ./src/SR1.html | sed -e 's/0060/0230/g' | sed -e 's/Station 1/Station 3/g' | sed -e 's/<a class="nav" href="SR3.html">SR3<\/a>|<a class="nav" href="SR2.html">SR2<\/a>/<a class="nav" href="SR1.html">SR1<\/a>|<a class="nav" href="SR2.html">SR2<\/a>/g' > ./src/SR3.html
sed 's/SR1/SR4/g' ./src/SR1.html | sed -e 's/0060/0310/g' | sed -e 's/Station 1/Station 4/g' | sed -e 's/<a class="nav" href="SR4.html">SR4<\/a>|<a class="nav" href="SR2.html">SR2<\/a>/<a class="nav" href="SR1.html">SR1<\/a>|<a class="nav" href="SR2.html">SR2<\/a>/g' > ./src/SR4.html
