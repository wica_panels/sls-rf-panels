# Overview

This log describes the functionality of tagged versions within the repository.

# Tags  

## [Version 1.0.0](https://gitlab.psi.ch/wica_panels/sls-rf-panels/tags/1.0.0)
  First tagged release. LAB1 (500 MHz), LAB2 (50 MHz) and TS (SLS test stand)
  available

## [Version 1.1.0](https://gitlab.psi.ch/wica_panels/sls-rf-panels/tags/1.1.0)
   Added support for custom text rendering on SR1 to SR4. Cleaned up plot handling.
   Release date 2024-10-25

## [Version 1.2.0](https://gitlab.psi.ch/wica_panels/sls-rf-panels/tags/1.2.0)
   - Re-generate SR2-4, add status message to BO
   - BO correct PV name
   - Back port module/script part from SR2
   - Some PV names fixed, A1/A2 added RFReference
   - Fix time axis unit label
   - Eliminate unneeded script block [in SPB.html].
   - Release date 2024-12-18

## [Version 1.2.1](https://gitlab.psi.ch/wica_panels/sls-rf-panels/tags/1.2.1)
   - Eliminate unneeded script block [in sls_rf_stations.html].
   - Release date 2024-12-18
