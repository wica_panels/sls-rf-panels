# Overview

This is the **sls-rf-panels** git repository which provides a set of wica panels for visualising relevant aspects of 
SLS state to the RF operators. 

Currently, this project is at the prototype stage used only to demonstrate the proof-of-concept. 

For further information please contact:
* [Roger Kalt](https://intranet.psi.ch/en/people/roger-kalt)
* [Simon Rees](https://intranet.psi.ch/en/people/simon-rees)

## Repository Organisation

The intention is simplicity and that you normally work on the **master branch** of the repository.

The following branches are additionally defined, but you don't normally need to look at them as they are used solely to manage
the autodeployment aspects.

* [dist_dev](https://gitlab.psi.ch/wica_panels/sf-op-panels/-/tree/dist_dev) - Use for deployments to [Wica Development Server](https://gfa-wica-dev.psi.ch)
* [dist_prod](https://gitlab.psi.ch/wica_panels/sf-op-panels/-/tree/dist_prod) - Use for deployments to [Wica Production Server](https://gfa-wica.psi.ch)
* [dist_ext](https://gitlab.psi.ch/wica_panels/sf-op-panels/-/tree/dist_ext) - Use for deployments to [Wica External Server](https://wica.psi.ch)

# How to Develop

The project is a vanilla HTML/JS/CSS project. There is no build system or special tooling.

This project supports three  dist areas.

* The dist_dev filesystem area is used for deploying to the [Wica Development Server](https://gfa-wica-dev.psi.ch/).
* The dist_prod filesystem area is used for deploying to the [Wica Production Server](https://gfa-wica.psi.ch/).
* The dist_ext filesystem area is used for deploying to the [Wica External Server](https://wica.psi.ch/).


## Setting up the Working Environment

You will need git command line tools and curl to trigger the autodeployment. Since there is no build system there
is not much to do. 

To cleanly handle the deployment to the different Wica servers using separate git branches some git command line
voodoo is required. The procedure is as follows:

Clone the project:
```
  mkdir my_dev_dir
  git clone https://gitlab.psi.ch/wica_panels/sls-rf-panels.git
```

Go into the project top-level directory and create directories which will track the remote deployment branches on
the GitLab Server.
```
  cd my_dev_dir
  mkdir dist_dev dist_prod dist_ext
```

Now associate these new directories with the remote branches on the server.
```
  git worktree add -f dist_dev dist_dev
  git worktree add -f dist_prod dist_prod
  git worktree add -f dist_ext dist_ext
```
You should see messages like the following:
```
  Preparing worktree (new branch 'dist_ext')
  branch 'dist_ext' set up to track 'origin/dist_ext'.
```

That's it. Go into the project's src directory and start editing !


## How to edit

Use your favourite editor to edit the source files in the /src directory.


## How to deploy

Use the `deploy_all.sh` or `deploy_dev.sh` scripts to execute the steps described below.

Note: the following steps are deliberately platform-agnostic. In practice you would want to automate them but the best
means to do this depends on your development platform.

When you are ready to deploy, copy the contents of the src directory to the git work tree branch associated with the
server where you wish to autodeploy.

```
  cp -a  src/* dist_dev
  cp -a  src/* dist_prod
  cp -a  src/* dist_ext
```

Then commit the files to the relevant remote branch (or branches) on the GitLab server.

```
  cd  dist_dev
  git commit -m "my changes" .  
  git push 
  
  cd  dist_prod
  git commit -m "my changes" .  
  git push 

  cd  dist_ext
  git commit -m "my changes" .  
  git push   
```

Trigger the relevant autodeployer 
```
  curl -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://gfa-wica-dev.psi.ch:8443/deploy?gitUrl=git@gitlab.psi.ch:wica_panels/sls-rf-panels.git"
  curl -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://gfa-wica.psi.ch:8443/deploy?gitUrl=git@gitlab.psi.ch:wica_panels/sls-rf-panels.git"
  curl -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://wica.psi.ch:8443/deploy?gitUrl=git@gitlab.psi.ch:wica_panels/sls-rf-panels.git"
```

If the autodeployer was successfully triggered you should see the following message:
```
  The deployment request was ACCEPTED and will be scheduled.
```

Now navigate your browser to the relevant URL and you can immediately review your changes:
```
https://gfa-wica-dev.psi.ch/sls/rf/example.html
https://gfa-wica.psi.ch/sls/rf/example.html
https://wica.psi.ch/sls/rf/example.html
```



# Project Changes and Tagged Releases

* See the [CHANGELOG.md](CHANGELOG.md) file for further information.



# References
* WICA repositories: https://github.com/paulscherrerinstitute?q=wica&type=all&language=&sort=
